# Sample Command Line App - with command argument parser

First of, this is just a sample command line parser application - and by no means not production quality code. You can use it as a sample to learn how you may approach designing your command line argument parser. But again, this isn't production quality code.

## Usage

![Usage](https://i.ibb.co/w4kMVF2/Usage.png)

The command line arguments **from** and **to** parameters are not required. When they are not provided, default data interval will be applied to filter data.

If those parameters are provided however, the following should be held true:

- the arguments can come in **any order**
- the value of **from** should be less than the value of **to**
- ***from*** param can be provided as either ***(--from, -from, --f, -f)***
- ***to*** param can be provided as either **(--to, -to, --t, -t)**
- both **from** and **to** command line arguments are not case sensitive too.
- ***Usage Info*** can be displayed by using either of the following options **(*--help*, *-help*, *--h*, *-h*)**.

**Note:**

All options aren't case sensitive.

