﻿using System;

namespace CommandLine
{
    public class CommandLineArg
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}
