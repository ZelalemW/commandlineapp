﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace CommandLine
{
    public class DBDataReader
    {
        private static string _connectionString = "<your db connection string goes here - for prod code, it should be kept in secret store such as Azure KeyVault.>";

        public static DataTable GetData(CommandLineArg commandLineArg)
        {
            DataTable dataTable = new DataTable();
            //for now - use this dummy collection
            PopulateDummyData(dataTable);

            //UNCOMMENT - the following code - and provide stored procedure that returns your data

            //using (var connection = new SqlConnection(_connectionString))
            //using (var command = new SqlCommand())
            //{
            //    command.Connection = connection;
            //    command.CommandType = CommandType.StoredProcedure;
            //    command.CommandText = "<your stored procedure name goes here>";
            //    command.Parameters.Add(new SqlParameter("@DateFrom", commandLineArg.From));
            //    command.Parameters.Add(new SqlParameter("@DateTo", commandLineArg.To));

            //    using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
            //    {
            //        dataAdapter.Fill(dataTable);
            //    }
            //}

            return dataTable;
        }

        private static void PopulateDummyData(DataTable dataTable)
        {
            dataTable.Columns.Add("Col 1", typeof(string));
            dataTable.Columns.Add("Another col", typeof(string));
            dataTable.Columns.Add("From", typeof(DateTime));
            dataTable.Columns.Add("To", typeof(DateTime));
            dataTable.Columns.Add("Count", typeof(int));

            dataTable.Rows.Add(new object[] { "Row 1 val", "Row 1-2 val", "1/2/2019", "12/3/2019", 34 });

            dataTable.Rows.Add(new object[] { "Row 2 val", "something else", "11/22/2017", "4/7/2018", 99 });

            dataTable.Rows.Add(new object[] { "Another row", "row3 col", "3/2/2019", "5/8/2019", 21 });
        }
    }
}
