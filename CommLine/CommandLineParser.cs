﻿using System;
using System.Collections.Generic;

namespace CommandLine
{
    public class CommandLineParser
    {
        private static char _parameterNameValueSeparator = ':';

        private static IDictionary<string, List<string>> _optionalParameters = new Dictionary<string, List<string>>
        {
            {"help", new List<string> {"--help", "-help", "--h", "-h" } },
            {"from", new List<string> {"--from", "-from", "--f", "-f" } },
            {"to", new List<string> {"--to", "-to", "--t", "-t" } }
        };


        public static void DisplayUsage(bool showIncorrectUsage = false)
        {
            if (showIncorrectUsage)
            {
                var currentTextColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect usage.");
                Console.ForegroundColor = currentTextColor;
            }

            Console.WriteLine("Usage: ");
            Console.WriteLine(".\\dotnet CommandLineApp.dll [--from: <starting date>, --to: <end date>]");
            Console.WriteLine("Example 1: .\\dotnet CommandLineApp.dll --from: 1/2/2019, --to: 3/7/2019");
            Console.WriteLine("Example 2: .\\dotnet CommandLineApp.dll");
            Console.WriteLine("Options: ");
            Console.WriteLine("\t --help");
            Console.WriteLine("\t --from: <starting date> (example: 1/2/2019)");
            Console.WriteLine("\t --to: <end date> (example: 3/7/2019)");
        }

        private static bool IsSeekingUsageInfo(string[] args)
        {
            return args.Length == 1 && _optionalParameters["help"].Contains(args[0].ToLower());
        }

        public static CommandLineArg ParseCommandLineArg(string[] args)
        {
            var commandLineArg = new CommandLineArg();
            var now = DateTime.UtcNow;

            if (args == null || args.Length == 0)
            {
                commandLineArg.To = now;
                commandLineArg.From = now.AddDays(-365); // default range - let it be 1 year.
            }
            else if (IsSeekingUsageInfo(args))
            {
                DisplayUsage(showIncorrectUsage: false);
                commandLineArg = null;
            }
            else if (args.Length == 2)
            {
                var arg1 = args[0]?.Trim();
                var arg2 = args[1]?.Trim();

                var arg1ParamNameValues = arg1.Split(_parameterNameValueSeparator);
                var arg2ParamNameValues = arg2.Split(_parameterNameValueSeparator);

                if (arg1ParamNameValues.Length != 2 || arg2ParamNameValues.Length != 2)
                {
                    DisplayUsage(showIncorrectUsage: true);
                    commandLineArg = null;
                }

                var param1Name = arg1ParamNameValues[0]?.Trim().ToLower();
                var param1Value = arg1ParamNameValues[1]?.Trim().ToLower();

                var param2Name = arg2ParamNameValues[0]?.Trim();
                var param2Value = arg2ParamNameValues[1]?.Trim();

                string from;
                string to;

                //optional parameters might come in any order.
                if (_optionalParameters["from"].Contains(param1Name) && _optionalParameters["to"].Contains(param2Name))
                {
                    from = param1Value;
                    to = param2Value;
                }
                else if (_optionalParameters["to"].Contains(param1Name) && _optionalParameters["from"].Contains(param2Name))
                {
                    from = param2Value;
                    to = param1Value;
                }
                else
                {
                    DisplayUsage(showIncorrectUsage: false);
                    commandLineArg = null;
                    return commandLineArg;
                }

                DateTime dateFrom;
                DateTime dateTo;

                if (DateTime.TryParse(from, out dateFrom) && DateTime.TryParse(to, out dateTo))
                {
                    commandLineArg.From = dateFrom;
                    commandLineArg.To = dateTo;
                }
                else
                {
                    DisplayUsage(showIncorrectUsage: true);
                    commandLineArg = null;
                }
            }
            else
            {
                DisplayUsage(showIncorrectUsage: true);
                commandLineArg = null;
            }

            return commandLineArg;
        }
    }
}
