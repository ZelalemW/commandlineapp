﻿using System;
using System.Data;
using System.IO;

namespace CommandLine
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandLineArg commandLineArg = CommandLineParser.ParseCommandLineArg(args);

            if (commandLineArg == null)
            {
                return;
            }

            Console.WriteLine($"Retriving Data from: {commandLineArg.From.ToString("MM-dd-yyyy")} to: {commandLineArg.To.ToString("MM-dd-yyyy")}");
            var watch = System.Diagnostics.Stopwatch.StartNew();
            DataTable dataTable = DBDataReader.GetData(commandLineArg);
            watch.Stop();

            Console.WriteLine($"Retrieved {dataTable.Rows.Count.ToString()} items.");

            if (dataTable.Rows.Count > 0)
            {
                var file = Path.Combine(Environment.CurrentDirectory, $"data_from_{commandLineArg?.From.ToString("MM-dd-yyyy")}_to_{commandLineArg?.To.ToString("MM-dd--yyyy")}.csv");
                Console.WriteLine($"Exporting data to: {file}");
                CsvExporter.Export(dataTable, file);
                Console.WriteLine($"Exporting complete.");
            }

            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine($"Command executed in {elapsedMs.ToString()} ms");

            Console.WriteLine("Press Enter to exit.");
            Console.ReadLine();
        }
    }
}
