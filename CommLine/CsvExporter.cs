﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace CommandLine
{
    public class CsvExporter
    {
        /// <summary>
        /// Exports the record as CSV file
        /// </summary>
        /// <param name="dataTable">input data in table form</param>
        /// <param name="file">file name</param>
        public static void Export(DataTable dataTable, string file)
        {
            StringBuilder sb = new StringBuilder();

            // GETs the table's COLUMNS Names. It will be used in the CSV file too.
            var header = string.Join(",", dataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName));
            sb.AppendLine(header);

            // GET each rows data in the table and build a list of column values - so that we will JOIN them with COMMA for our file.
            // NOTE: - if any of column data might come with COMMA with its value 
            // - then, that will malform the CSV file. You can either wrap the value using [] or anything to make sure it belong to one column.
            // DEAL WITH IT AS YOU SEE FIT FOR YOUR NEED :) just a precaution - to think about - especially when you later attempt to process the file
            // consider rows with extra COMMAs than expected.
            foreach (DataRow row in dataTable.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());

                sb.AppendLine(string.Join(",", fields));
            }

            if (File.Exists(file))
            {
                file = $"{Path.GetFileNameWithoutExtension(file)}-{DateTime.Now.ToString("hh-mm-ss-fff")}{Path.GetExtension(file)}";
            }

            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine(sb.ToString());
            }
        }
    }
}
